# JavaScript

## Introduction

Javascript is a very ubiquitous language in the field of web development, especially in giving life to webpages and web apps. Recent development of tools and NodeJS allow Javascript to be used even in making server-side apps, native apps, and even mobile apps.

## Useful Links and Tutorials

### Quick Start

[W3 Javascript tutorial](https://www.w3schools.com/js/)
[CodeAcademy Javascript tutorial](https://www.codecademy.com/learn/introduction-to-javascript)

### Cheat Sheets

[<MyFavorite/> Javascript Cheat Sheet](https://www.onblastblog.com/javascript-cheat-sheet/)
[<MyFavorite/> HTML Cheat Sheet](https://www.onblastblog.com/javascript-cheat-sheet/)
[Codementor Javascript Cheat Sheet](https://www.codementor.io/johnnyb/javascript-cheatsheet-fb54lz08k)
[Another Cheat Sheet](https://goo.gl/o1fyvX)
[Cheatography Javascript Cheat Sheet](https://www.cheatography.com/davechild/cheat-sheets/javascript/)

# JQuery

JQuery is a very useful and widely-used JS library that helps make writing JS code for webpages much easier.

## Useful Links and Tutorials

* [Try.JQuery](http://try.jquery.com/) is a very useful tutorial on JQuery.
* Lecture materials for DevCamp 1718A on JQuery can be found [here.](https://gitlab.com/marcteves/lect)
  * Instructions on how to use the Git repository to study JQuery can be found in the README.
* An exercise on selectors can be found [here.](https://gitlab.com/marcteves/land)

# NodeJS (introduction to advanced JS)

[Node.js tutorial](https://nodeschool.io/)
After learning the basics, you can skip the _javascripting_ tutorial, and proceed with _learnyounode_.

>Credits: Adel Mandanas
