## Other Instructions:
https://gist.github.com/d2s/372b5943bce17b964a79

## Instructions:
```bash
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.4/install.sh | bash
```
Find out Node versions:
```bash
nvm ls
```
To install versions:
```bash
nvm install v[number]
```
Time of writing, latest LTS: v6.11.3, current: v8.5.0 
