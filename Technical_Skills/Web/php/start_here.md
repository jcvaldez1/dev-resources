# PHP

## Introduction

PHP is a server-side language that is widely-used in the industry. The demand is high, and thus learning this language and the tools that come with and use it is a good investment.

## Installation

* Install [XAMPP](http://www.wikihow.com/Install-XAMPP-on-Linux) on Linux. Follow the tutorial until part 2, step 2. Don't mind the images.
* Install [XAMPP](https://www.wikihow.com/Install-XAMPP-for-Windows) on Windows
* Install [XAMPP](https://imacify.com/2015/06/how-to-install-xampp-in-mac-osx-and-configure-virtual-host-in-it/) on Mac. Follow the steps until 'Steps to Start Apache Server in local machine'.

Please take note of the default location of XAMPP htdocs folder:
* Linux - `\opt\lampp`
* Windows - `C:\xampp`
* Mac - `\Applications\XAMPP`

One shall create PHP projects in these locations. Remember to create a separate folder for each project! You're now ready to learn PHP. See tutorial links below.

## Create your working directory
Follow the steps indicated [here](http://www.tutorialspoint.com/articles/run-a-php-program-in-xampp-server). Start at step 2 on creating a new folder under `htdocs` folder.

Tip: when you rename `add.php` to `index.php`, accessing `http://localhost/tutorialspoint` on the browser will automatically execute `index.php` (without explicitly pointing to `http://localhost/tutorialspoint/index.php`). The index file usually contains the home page of your application, or some initialization scripts when working under a PHP framework (e.g. WordPress, Laravel).

## Useful Links and Tutorials
These tutorials assume that the user already set up his working directory. Make sure to finish the first tutorial before going to next. Be sure to ready your preferred text editor!

* Learn PHP [interactively](http://www.learn-php.org/)!
* PHP [tutorial](https://www.tutorialspoint.com/php/)
* PHP 5 [tutorial](https://www.w3schools.com/php/)
* [PHP cheat sheet](https://blueblots.com/development/php-cheat-sheets/)

## Test Yourself!

* Finish the basic PHP quiz [here](http://thephpbasics.com/quiz-yourself/).
* Finish another basic PHP quiz [here](https://www.w3schools.com/php/php_quiz.asp).

# Wordpress

## Introduction

PHP is most often used in Wordpress, which is very common in making company websites and blogs, or anywhere where there is a need to manage content like posts and images.

## Installation

* Install WordPress on top of [XAMPP](https://premium.wpmudev.org/blog/setting-up-xampp/). Proceed immediately to 'Setting Up Your MySQL Database'. To access PHPmyAdmin interface just open your browser and go to http://localhost/phpmyadmin and follow the steps indicated.
* Install WordPress on top of [LAMP](https://www.tecmint.com/install-wordpress-on-ubuntu-16-04-with-lamp/), *for reference. We will be using XAMPP.*

Way to go! To learn more about using WordPress, read the tutorial links below.

## Useful Links and Tutorials

* Get the most from [WordPress.com](https://learn.wordpress.com/)!
* Using [WordPress](https://www.tutorialspoint.com/wordpress/)
* 25 [brilliant](http://www.creativebloq.com/web-design/wordpress-tutorials-designers-1012990) WordPress tips and tricks
* You think you already know WordPress? Then try building your own shopping cart application using WooCommerce plugin [features](https://slicejack.com/woocommerce-beginners/) and tutorials  [here](https://docs.woocommerce.com/documentation/plugins/woocommerce/getting-started/) and [here](https://websitesetup.org/start-online-store/).
* Custom theme on [WordPress](https://codex.wordpress.org/Theme_Development). This tutorial assumes the reader with **good knowledge** of PHP.

>Credits: Frank Rayo
