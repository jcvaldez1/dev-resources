# Learn Ruby on Rails

This guide is for everyone who wants to learn [Ruby on Rails](http://www.rubyonrails.org). sqlite3 is the suggested database management system in this guide. Moreover, this will include our recommended environment and links for the most reliable and best tutorials on web.

## Linux

**Supported Distributions**
1. Ubuntu (Precise Pangolin and Trusty Tahr)
2. ElementaryOS (Luna)
3. Linux Mint (Cinnamon)

#### Dependencies

Install the following dependencies:

* git
* sqlite3
* nodeJs
* Rbenv
* Ruby
* Rails
* Bundle

Install **git**, **sqlite3**, **nodeJS**:
```bash
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libpq-dev libgdbm3 libgdbm-dev
sudo apt-get install sqlite3 libsqlite3-dev
sudo apt-get install git nodejs
```

For a recommended **nodeJS** *version manager* installation, check [NVM](https://github.com/creationix/nvm).

Note: If this is your first time on git, it's advisable to configure your credentials first:
```bash
git config --global user.name "Neil Calabroso"
git config --global user.email "nmcalabroso@up.edu.ph"
```

Also, make sure your git's core.autocrlf setting is set to false to avoid any problems when installing rbenv.
```bash
git config --global core.autocrlf false
```

Install [Rbenv](https://github.com/sstephenson/rbenv). Complete the installation instructions including the optional part:```ruby-build```. We [recommend](http://jonathan-jackson.net/rvm-and-rbenv) using *rbenv* over *RVM*. :)

Upon installing **Rbenv**, restart your terminal and install Ruby 2.4.0:
```bash
rbenv install 2.4.0
rbenv global 2.4.0
```

Install **Rails**:
```bash
gem install rails
```

Install Bundler:
```bash
gem install bundler #restart terminal after the installation
```

Create a new Rails app and install dependencies:
```bash
rails new app_name
cd app_name
bundle install
```

If when bundling an error occurs on installing [puma](https://github.com/puma/puma/issues/1136), uninstall libssl-dev then install libssl1.0-dev then try bundling again.
```bash
sudo apt-get uninstall libssl-dev
sudo apt-get install libssl1.0-dev
bundle install
```

## Rails 5
* Rails [book](https://www.railstutorial.org/book)
* Building the [perfect](http://sourcey.com/building-the-prefect-rails-5-api-only-app/) rails 5 api only app
* Building a JSON API with [Rails](https://blog.codeship.com/building-a-json-api-with-rails-5/) 5
* How to make rails 5 app [api-only](https://hashrocket.com/blog/posts/how-to-make-rails-5-api-only)
* Creating Rails 5 API only application following JSON:API [specification](https://www.simplify.ba/articles/2016/06/18/creating-rails5-api-only-application-following-jsonapi-specification/)
* Token-based authentication with Ruby on Rails 5 [API](http://tutorials.pluralsight.com/ruby-ruby-on-rails/token-based-authentication-with-ruby-on-rails-5-api)

## Rails 4
* Rails-api [docs](https://github.com/rails-api/rails-api)
* Creating a Simple API with [Rails](https://www.codementor.io/ruby-on-rails/tutorial/creating-simple-api-with-rails). One still need to install *rails-api* gem first, but for rails 5, this gem is already included.
* APIs on Rails [book](http://apionrails.icalialabs.com/book/chapter_one)

### Tutorials
* If you do not have any experience on ruby, we suggest to complete the challenge at [TryRuby](http://www.tryruby.org).
* Learn Ruby the Hard [Way](http://learnrubythehardway.org/book/ex32.html). *Challenge accepted!*
* With the set-up above, you are now ready for your [first web app tutorial](http://guides.rubyonrails.org/getting_started.html). You can now start at *section 3.2*.
* Since the [Getting Started Tutorial](http://www.guides.rubyonrails.org/getting_started.html) is too high-level (with the use of scaffolding), it is very much suggested to read and finish the book [Ruby on Rails Tutorial](http://www.rubyonrailstutorial.org/book). This book gives a thorough explanation about the workflow of **Rails**, **Unit-testing**, **Git**, and **Deployment**.

### Further Notes

For a high quality and readable codebase, please make these conventions as references:

0. [Ruby Style Guide](https://github.com/bbatsov/ruby-style-guide)
1. [Rails Style Guide](https://github.com/bbatsov/rails-style-guide)

## Useful gems in Rails

1. [SemanticUI](https://github.com/doabit/semantic-ui-sass). Learn more about using SemanticUI [here](http://semantic-ui.com/).
2. [haml-rails](https://github.com/indirect/haml-rails). Learn more about using HAML [here](http://haml.info/docs/yardoc/file.REFERENCE.html).
3. How to use [slim](https://github.com/slim-template/slim)?
4. Installing [slim](https://github.com/slim-template/slim-rails) in Rails

## Deploying Rails app using Heroku

1. [Heroku](https://devcenter.heroku.com/search?q=deploy+rails)
