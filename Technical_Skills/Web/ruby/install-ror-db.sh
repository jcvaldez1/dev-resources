#For DCS TL2 use only! 32-bit Linux Mint with proxy
#Innov School 4.0: Ruby Rush
#J Franco Ray, Feb 25, '16

cd

#Install PostgreSQL
sudo apt-get install -y postgresql
sudo apt-get install -y postgresql-9.4
#Run: psql

#Install MongoDB
sudo apt-get install -y mongodb
#Install MongoDB Community version
curl -O https://fastdl.mongodb.org/linux/mongodb-linux-i686-3.2.3.tgz
tar -zxvf mongodb-linux-i686-3.2.3.tgz
rm mongodb-linux-i686-3.2.3.tgz
mkdir -p ~/.mongodb
cp -R -n mongodb-linux-i686-3.2.3/ ~/.mongodb
rm -rf mongodb-linux-i686-3.2.3/
echo 'export PATH="$HOME/.mongodb/bin:$PATH"' >> ~/.bashrc

#Running: sudo service mongod start/stop/restart

#Redis
wget http://download.redis.io/releases/redis-3.0.7.tar.gz
tar -xzf redis-3.0.7.tar.gz
rm redis-3.0.7.tar.gz
cd redis-3.0.7/
sudo make
sudo make install
echo 'export PATH="$HOME/redis-3.0.7/src:$PATH"' >> ~/.bashrc
#Run: src/redis-server
#Interact: src/redis-cli -> set foo bar -> get foo -> 'bar'
