# Python Resources

## Python
### Beginner Tutorials and Cheatsheets

* [Youtube Tutorial](https://www.youtube.com/watch?v=N4mEzFDjqtA)
* [Python3 in One Pic](https://raw.githubusercontent.com/coodict/python3-in-one-pic/master/py3%20in%20one%20pic.png)
  * Just one huge PNG image summarizing all of Python3's syntax
* [Python Notes PDF](https://drive.google.com/file/d/0B30BxjRT6yhdWEVMSXQwaExYSDA/view)
  * A more detailed cheatsheet

### Notes

* We recommend [Sublime Text](https://www.sublimetext.com/), a text editor that runs fast and has many nice features.
  * We also recommend [Atom](https://atom.io), as it also has many nice features and is open source, but it's somewhat slower to start.
* If you have no experience on HTML and CSS, read up on them, and try to make a really simple webpage. No need to delve into it, just learn the basics.

## Django
### Tutorials and Cheatsheets

* The [Official Django tutorial](https://docs.djangoproject.com/en/1.11/intro/tutorial01/) is useful to learning the framework.
* There are also notes from DevCamp 1718A that are based on the tutorial, but include additional notes and rephrasing to ease beginners.
  * [DevCamp: Python Handout](https://drive.google.com/open?id=13g7dLt43hps3sBT-3nXky7p6MdcsyBO-DBBp34g8-sA)
  * [DevCamp: Python Legit Notes](https://drive.google.com/open?id=1b-QDQP-TCMJiqbuyJfCk1NbqUEgUxk8vObzj-4ci7F8)
  * [DevCamp: Python Personal Notes](https://drive.google.com/open?id=1UH-3GH7AD9VeuHszd_Px3ozBip373aIRsGYBcC-6nuQ)

>Credits: Carl Araya
