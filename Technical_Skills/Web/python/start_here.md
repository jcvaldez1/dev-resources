# Python

## Introduction

Python is very widely used in fields ranging from scientific computing to web development, and the ecosystem of packages in Python is very rich.

## Installation
If on Windows, [download and install the latest version of Python 3.](https://www.python.org/downloads/)

If on Linux, it will most likely be installed by default. To update the version, just run this on the terminal:
```
sudo apt-get update
sudo apt-get install python3
```

> Note: The installation process will differ based on the user's system and the version of Python desired. We suggest you research if you want something different. Feel free to contact us if you need help.

## Tutorials

Check [here](https://gitlab.com/up-csi/dev-resources/blob/master/Technical_Skills/Web/python/resources.md) for some resources on starting on Python (specifically Python3) and Django.

## Other tools

### Pip

Pip is Python's default package manager, and is used to install Python packages for things like web development, game development, and linear algebra, among many others.

### Virtualenv

Virtualenv is used to create virtual Python environments. Imagine having 2 projects run in different versions of Python3, and using different versions of the same package. Virtualenv isolates projects and dependencies like these.

### Virtualenvwrapper

Virtualenvwrapper, as the name implies, is a wrapper / helper for Virtualenv, and helps manage different virtual environments. You can read more about it [here.](https://virtualenvwrapper.readthedocs.io/en/latest/)

# Django

Django is a Python framework used to easily make web applications. It is similar to Rails in terms of the product app's architecture, but they have their difference in design philosophies.

## Tutorials and Installation of Django

These instructions are meant to serve as general instructions to installing Django, and as such make assumptions about the system of the user and the programs installed. If you want more specific instructions, [check out this installation instructions from last year's DevCamp.](https://gitlab.com/up-csi/dev-resources/blob/master/Technical_Skills/Web/python/django_install_instructions.md)

### Windows

[Follow the steps shown here.](https://docs.djangoproject.com/en/1.11/howto/windows/)

### Linux
> Note: The steps here are largely similar to that of installing Python and any Python packages.

> Note: When you see `$ ` before a line of command, that indicates that that line should be ran in the Terminal. When copy-pasting, ignore the `$ `.

Make sure `python3`, `pip3`, and `virtualenv` are installed in your computer. First, install `pip3` then install `virtualenv` through `pip3`:
```
$ sudo apt-get install python3-pip
$ sudo pip3 install virtualenv
```
Note that `sudo pip3` usually uses the global version of `pip3` and `python3`, which may differ from the one used when no `sudo` is used. Compare `sudo which pip3` and `which pip3` to check.
> Sidenote: Redundancies like this are why virtual environments are used, but in this case they have to be installed first.

Then, create the virtual environment. Go to your `workspace` or other work folder, then create it there:
```
$ mkdir ~/workspace
$ cd ~/workspace
$ virtualenv .venv -p python3
  # .venv is the name of the virtual environment
  # -p is the flag that indicates which Python to use in .venv
```

To enter the environment, run:
```
source .venv/bin/activate
```

Your terminal prompt should look something like this:
```
(.venv) $ _
```

Install packages while in the environment:
```
$ pip install django
```
Note that the `pip` executable in the environment should be used, to keep the installation of packages local to the environment, and to avoid version and dependency issues with this or other Python projects. to check, run `which pip` or `whereis pip`.

Check if Django is installed correctly by running:
```
$ django-admin --version
```

To exit from the environment:
```
$ deactivate
```

>Credits: Carl Araya
