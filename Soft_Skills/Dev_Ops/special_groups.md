# Let's level up for more diverse and specialized skillset!

## Resources for Special Interests

Resources are available at **CSI Library**.

Here is the preliminary [list](https://drive.google.com/folderview?id=0B3YLa4xRWVTLa1lHUC1SUGVNdjg&usp=sharing) of books. One can PM me at FB to request a copy.

Note: **Some books listed below but without links are located [here](https://drive.google.com/open?id=0B9147eOtQN5xTTBSV2tieDY0QlE).**

## Computer security

0. Rails [security](http://guides.rubyonrails.org/security.html)
1. Django [security](https://docs.djangoproject.com/en/1.9/topics/security/)
2. Web Penetration Testing with Kali Linux
3. Ethical [guide](http://darkcoder.in/ethical-guide-for-hackers/) for hackers
4. [hacklib](https://github.com/leonli96/python-hacklib) - pentesting, port scanning, and logging in anywhere with Python

## Scientific Libraries

### Ruby
0. [SciRuby](http://sciruby.com)
1. [Ai4Ruby](http://ai4r.org)
2. [Creosote](http://srawlins.ruhoh.com/)
3. [matlab-ruby](http://matlab-ruby.rubyforge.org/matlab-ruby/)
4. [ruby-OpenCV](https://github.com/ruby-opencv/ruby-opencv)
5. [ruby fann](http://ruby-fann.rubyforge.org/)
6. [BioRuby](http://bioruby.org/)
7. ...

### Python
0. [SciPy](http://scipy.org)
1. [Fenics](http://fenicsproject.org)
2. [jupyter](http://jupyter.org)
3. [SageMath](http://sagemath.org)
4. Numeric and Scientific [Libraries](https://wiki.python.org/moin/NumericAndScientific) in Python
5. Numeric and Scientific [Libraries](https://wiki.python.org/moin/NumericAndScientific/Libraries) in Python 2
6. Numeric and Scientific [Libraries](https://github.com/svaksha/pythonidae/blob/master/AI.md)

## Algorithms and Scientific Computing
0. A Primer on Scientific Programming using Python, 3e
1. ...

### Bonus: Competitive Programming (for brave souls!)
0. Programming Challenges - Steven Skiena
1. Codility.com [challenges](https://codility.com/programmers/challenges/)
2. Project Euler, [of course!](https://projecteuler.net/)
3. ...

## Data Science and Artificial Intelligence
0. Data Mining - Practical Machine Learning Tools and Techniques
1. Data Mining for Dummies
2. ...

### Bonus: Data Visualization (JavaScript-based)
0. [d3.js](http://d3js.org)
1. [HighCharts](http://highcharts.com)
2. Ask [Google](https://www.google.com/search?q=ruby+social+network+analysis&ie=utf-8&oe=utf-8#q=javascript+data+visualization)
3. ...

## Computer Systems and Networks
...

## Human-Computer Interaction

### Web Design
0. [TutsPlus](http://webdesign.tutsplus.com/)
1. [WebDesignerMag](http://www.webdesignermag.co.uk/)
2. [TeamTreeHouse](https://teamtreehouse.com/tracks)
3. Some [tutorials](http://www.hongkiat.com/blog/responsive-web-tutorials/)
4. Some [tutorials](http://www.creativebloq.com/rwd/responsive-web-design-tutorials-71410085) 2
...

## Web and Mobile Application Development
...
